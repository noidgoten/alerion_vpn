sudo apt install -y libssl-dev  liblzo2-dev libpam0g-dev git
wget https://swupdate.openvpn.org/community/releases/openvpn-2.3.11.tar.gz
wget https://swupdate.openvpn.org/community/releases/openvpn-2.3.11.tar.gz.asc
wget http://swupdate.openvpn.net/community/keys/samuli_public_key.asc
gpg --import samuli_public_key.asc
gpg -v --verify openvpn-2.3.11.tar.gz.asc
tar -xzf openvpn-2.3.11.tar.gz
rm openvpn-2.3.11.tar.gz.asc openvpn-2.3.11.tar.gz samuli_public_key.asc
cd openvpn-2.3.11
./configure
make
sudo make install
git clone https://github.com/OpenVPN/easy-rsa
cd easy-rsa/easyrsa3
./easyrsa init-pki
./easyrsa build-ca
./easyrsa build-server-full server
./easyrsa build-client-full client1
./easyrsa build-client-full client2
./easyrsa gen-dh
